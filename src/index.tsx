import React from "react";
import ReactDOM from "react-dom";

import "./extensions/Array";
import "./extensions/Map";
import "./extensions/Set";
import "./extensions/String";

import App from "./App";

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById("root")
);
