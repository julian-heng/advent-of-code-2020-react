export function cartesian<T>(...a: Array<Array<T>>): Array<Array<T>>
{
    return a.reduce<Array<Array<T>>>((results, entries) => {
        return results.map(r => entries.map(e => r.concat([e])))
                      .reduce((sub, r) => sub.concat(r), []);
    }, [[]]);
}


export function range(start: number, stop: number, step: number = 1):
    Array<number>
{
    let length = Math.ceil((stop - start) / step);
    return Array(length).fill(start)
                        .map((x, y) => x + y * step);
}


export function inBounds<T>(grid: Array<Array<T>>, row: number, col: number):
    boolean
{
    return 0 <= row && row < grid.length && 0 <= col && col < grid[row].length;
}


export function counter<T>(a: Array<T>): Map<T, number>
{
    let count = new Map<T, number>();
    a.forEach((e: T) => {
        let tally = count.get(e);
        count.set(e, (tally === undefined ? 0 : tally) + 1);
    })
    return count;
}
