import React from "react";
import Container from "react-bootstrap/Container";

import SolutionComponent from "./components/SolutionComponent";
import NavigationComponent from "./components/NavigationComponent";

import * as Solutions from "./solutions";
import { getSolutionBackend } from "./solutions";

import "bootstrap/dist/css/bootstrap.min.css";


type AppState = {
    selected: number;
    backend: () => Solutions.DayX | undefined,
};


class App extends React.Component<{}, AppState>
{
    static defaultState: AppState = {
        selected: 1,
        backend: getSolutionBackend(1),
    };

    private solutionElement = React.createRef<SolutionComponent>();

    constructor(props: Object)
    {
        super(props);

        this.state = App.defaultState;
        this.handle = this.handle.bind(this);
        this.solutionElement = React.createRef<SolutionComponent>();
    }

    render()
    {
        return (
            <Container fluid className="App">
                <NavigationComponent
                    selected={this.state.selected}
                    handle={this.handle}
                />
                <SolutionComponent
                    ref={this.solutionElement}
                    backend={this.state.backend}
                    placeholder={Solutions.getInput(this.state.selected)}
                />
            </Container>
        );
    }

    private handle(event: string | null): void
    {
        switch (event)
        {
        case "ignore":
            break;

        case "reset":
            this.resetSolutionComponent();
            break;

        default:
            let num = Number(event);
            if (isNaN(num))
            {
                // unknown event
            }

            this.setState({
                selected: num,
                backend: getSolutionBackend(num),
            }, this.resetSolutionComponent);

            break;
        }
    }

    private resetSolutionComponent(): void
    {
        let node = this.solutionElement.current;
        if (node !== null)
        {
            node.reset();
        }
    }
}


export default App;
