import DayX, { DayXSolution } from "./DayX";


type Match = {
    result: boolean;
    offset: number;
};


class Day19 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb`;
    }

    protected _parseInput(_input: string): any
    {
        let [rulesStr, messagesStr] = _input.splitLinesMultiple();
        let rulesArr = rulesStr.splitLines().filter(Boolean);
        let messages = messagesStr.splitLines().filter(Boolean);

        let rules1 = new Map<string, Array<Array<string>>>(rulesArr.map(i => {
            let [key, ruleStr] = i.replaceAll("\"", "").split(": ", 2);
            let rule = ruleStr.split(" | ").map(i => i.split(/\s+/));
            return [key, rule]
        }));

        let rules2 = new Map<string, Array<Array<string>>>();
        rules1.forEach((v, k) => rules2.set(k, v));
        rules2.set("8", [["42"], ["42", "8"]]);
        rules2.set("11", [["42", "31"], ["42", "11", "31"]]);

        return [rules1, rules2, messages];
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let [rules1,, messages] = input;
        let result = Day19.solve(rules1, messages);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let [, rules2, messages] = input;
        let result = Day19.solve(rules2, messages);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve(
        rules: Map<string, Array<Array<string>>>,
        messages: Array<string>
    ): number
    {
        return messages.map(i => Day19.match(rules, "0", i))
                       .filter(Boolean)
                       .length;
    }

    private static match(
        rules: Map<string, Array<Array<string>>>,
        rule: string,
        s: string
    ): boolean
    {
        let { result, offset } = Day19._match(rules, rule, s);
        return result && offset === s.length;
    }

    private static _match(
        rules: Map<string, Array<Array<string>>>,
        rule: string,
        s: string
    ): Match
    {
        if (s.length === 0)
        {
            return {
                result: true,
                offset: 0,
            };
        }

        if (rule === "a" || rule === "b")
        {
            return {
                result: s.charAt(0) === rule,
                offset: Number(s.charAt(0) === rule),
            };
        }

        let valid = false;
        let offset = 0;

        for (let r of rules.getOrDefault(rule, []))
        {
            let ruleValid = true;
            offset = 0;

            for (let sr of r)
            {
                let next = s.slice(offset, s.length);
                let { result, offset: n } = Day19._match(rules, sr, next);

                offset += n;
                if (result && s.slice(offset, s.length).length === 0)
                {
                    if (sr !== rule && sr !== r[r.length - 1])
                    {
                        result = false;
                    }
                }

                if (!result)
                {
                    ruleValid = false;
                    break;
                }
            }

            if (ruleValid)
            {
                valid = true
                break;
            }
        }

        return {
            result: valid,
            offset: offset,
        };
    }
}


export default Day19;
