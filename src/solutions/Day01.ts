import DayX, { DayXSolution } from "./DayX";


type Day01Solution = {
    value: number;
    found: boolean;
};


class Day01 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `1721
979
366
299
675
1456`;
    }

    protected _parseInput(_input: string): any
    {
        return new Set(_input.splitLines()
                             .map(i => i.trim())
                             .filter(Boolean)
                             .map(Number));
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        return {
            result: Day01.solve(input).value,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let i = 0;
        for (i of input)
        {
            let target = 2020 - i;
            let result = Day01.solve(input, target);

            if (result.found)
            {
                return {
                    result: i * result.value,
                    auxilary: undefined,
                }
            }
        }

        return {
            result: 0,
            auxilary: undefined,
        };
    }

    private static solve(nums: Set<number>, target: number = 2020):
        Day01Solution
    {
        // eslint-disable-next-line
        for (let it = nums.values(), i = null; i = it.next().value; )
        {
            let j = target - i;
            if (nums.has(j))
            {
                return {
                    value: i * j,
                    found: true,
                }
            }
        }

        return {
            value: 0,
            found: false,
        };
    }
}


export default Day01;
