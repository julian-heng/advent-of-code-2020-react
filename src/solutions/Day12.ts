import DayX, { DayXSolution } from "./DayX";


const DIRECTION_VALID = [
    "N", "E", "S", "W"
] as const;

const ACTION_VALID = [
    ...DIRECTION_VALID, "L", "R", "F"
] as const;

type Direction = typeof DIRECTION_VALID[number];
type Action = typeof ACTION_VALID[number];
type Instruction = {
    action: Action,
    magnitude: number
};


class Day12 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `F10
N3
F7
R90
F11`;
    }

    protected _parseInput(_input: string): any
    {
        return _input.splitLines()
                     .map(i => i.trim())
                     .filter(Boolean)
                     .map(Day12.makeInstruction)
                     .filter(Boolean);
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;

        let directionSet = new Set(DIRECTION_VALID);
        let rotateSet = new Set(["L", "R"]);

        let distance = new Map<string, number>();
        let currentDirection: Direction = "E";
        let index = DIRECTION_VALID.indexOf(currentDirection);

        for (let {action, magnitude} of input)
        {
            if (directionSet.has(action))
            {
                magnitude += distance.getOrDefault(action, 0);
                distance.set(action, magnitude);
            }
            else if (rotateSet.has(action))
            {
                let indexOffset = Math.trunc(magnitude / 90);
                if (action === "L")
                {
                    indexOffset = DIRECTION_VALID.length - indexOffset;
                }

                index = (index + indexOffset) % DIRECTION_VALID.length;
                currentDirection = DIRECTION_VALID[index];
            }
            else if (action === "F")
            {
                magnitude += distance.getOrDefault(currentDirection, 0);
                distance.set(currentDirection, magnitude);
            }
        }

        let a = distance.getOrDefault("N", 0) - distance.getOrDefault("S", 0);
        let b = distance.getOrDefault("E", 0) - distance.getOrDefault("W", 0);
        result = Math.abs(a) + Math.abs(b);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;

        let directionSet = new Set(DIRECTION_VALID);
        let rotateSet = new Set(["L", "R"]);

        let distance = new Map<string, number>();
        let waypoint = new Map<string, number>([
            ["N", 1],
            ["E", 10],
            ["S", 0],
            ["W", 0],
        ]);

        for (let {action, magnitude} of input)
        {
            if (directionSet.has(action))
            {
                let wDistance = waypoint.getOrDefault(action, 0);
                wDistance += magnitude;
                waypoint.set(action, wDistance);
            }
            else if (rotateSet.has(action))
            {
                let vals = Array.from(waypoint.values());
                let indexOffset = Math.trunc(magnitude / 90);
                if (action === "L")
                {
                    indexOffset *= -1;
                }

                vals.rotate(indexOffset);

                let index = 0;
                waypoint.forEach((_: number, k: string) => {
                    waypoint.set(k, vals[index++]);
                });
            }
            else if (action === "F")
            {
                waypoint.forEach((v: number, k: string) => {
                    let kDistance = distance.getOrDefault(k, 0);
                    kDistance += (magnitude * v)
                    distance.set(k, kDistance);
                });
            }
        }

        let a = distance.getOrDefault("N", 0) - distance.getOrDefault("S", 0);
        let b = distance.getOrDefault("E", 0) - distance.getOrDefault("W", 0);
        result = Math.abs(a) + Math.abs(b);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static makeInstruction(line: string): Instruction | undefined
    {
        let m = line.match(/(\w)(\d+)/);

        if (m === null)
        {
            return undefined;
        }

        let [, _action, _magnitude] = m;
        let action = _action as Action;
        let magnitude = Number(_magnitude);

        if (!new Set(ACTION_VALID).has(action))
        {
            return undefined;
        }

        if (isNaN(magnitude))
        {
            return undefined;
        }

        return {
            action: action,
            magnitude: magnitude,
        };
    }
}


export default Day12;
