import DayX, { DayXSolution } from "./DayX";


type Passport = {
    [key: string]: string | number | undefined;
    byr?: number;
    iyr?: number;
    eyr?: number;
    hgt?: string;
    hcl?: string;
    ecl?: string;
    pid?: string;
    cid?: string;
};


type PassportChecks = {
    [key: string]: (v: any) => boolean;
}

const PASSPORT_REQUIRED_FIELDS: Array<string> = [
    "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"
];


const EYE_COLOURS: Set<string> = new Set([
    "amb", "blu", "brn", "gry", "grn", "hzl", "oth"
]);


const PASSPORT_CHECKS: PassportChecks = {
    byr: (v: number) => 1920 <= v && v <= 2002,
    iyr: (v: number) => 2010 <= v && v <= 2020,
    eyr: (v: number) => 2020 <= v && v <= 2030,
    hgt: (v: string) => {
        let num = Number(v.slice(0, v.length - 2));
        if (v.endsWith("cm"))
        {
            return 150 <= num && num <= 193;
        }
        else
        {
            return 59 <= num && num <= 76;
        }
    },
    hcl: (v: string) => /^#[0-9A-Za-z]{6}$/.test(v),
    ecl: (v: string) => EYE_COLOURS.has(v),
    pid: (v: string) => /^\d{9}$/.test(v),
    cid: (v: string) => true,
};


class Day04 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in`;
    }

    protected _parseInput(_input: string): any
    {
        let input = _input.splitLinesMultiple();
        let re = /(\w+):(#?\w+)/g;
        let passports = input.map(i => {
            let passport: Passport = {};

            Array.from(i.matchAll(re)).forEach(j => {
                switch (j[1])
                {
                case "byr": case "iyr": case "eyr":
                    passport[j[1]] = Number(j[2]);
                    break;
                case "hgt": case "hcl": case "ecl": case "pid": case "cid":
                    passport[j[1]] = j[2];
                    break;
                }
            });
            return passport;
        });
        return passports
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = input.filter((i: Passport) => {
            return !PASSPORT_REQUIRED_FIELDS.map(j => i[j])
                                            .some(j => j === undefined);
        });

        return {
            result: result.length,
            auxilary: result,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = auxilary.filter(Day04.checkPassport);

        return {
            result: result.length,
            auxilary: result,
        };
    }

    private static checkPassport(p: Passport): boolean
    {
        return PASSPORT_REQUIRED_FIELDS.map(i => PASSPORT_CHECKS[i](p[i]))
                                       .every(i => i);
    }
}


export default Day04;
