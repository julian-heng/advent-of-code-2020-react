import DayX, { DayXSolution } from "./DayX";


type Bus = {
    id: number;
    index: number;
};


class Day13 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `939
7,13,x,x,59,x,31,19`;
    }

    protected _parseInput(_input: string): any
    {
        let [_target, rest] = _input.splitLines(2)
        let target = Number(_target);
        let buses = rest.split(",")
                        .map(Day13.makeBus)
                        .filter(Boolean);
        return { target: target, buses: buses };
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;

        let { target, buses } = input;
        let bestTime = Number.MAX_SAFE_INTEGER;
        let bestId = 0;

        for (let { id } of buses)
        {
            let time = (id * Math.round(target / id)) - target;

            if (time < 0)
            {
                continue;
            }

            if (time < bestTime)
            {
                bestTime = time;
                bestId = id;
            }
        }

        result = bestTime * bestId;

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;

        let { buses } = input;
        let n = new Array<number>();
        let a = new Array<number>();

        buses.forEach(({ id, index }: Bus) => {
            n.push(id);
            a.push(id - index);
        });

        result = Day13.crt(n, a);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static makeBus(_id: string, i: number): Bus | undefined
    {
        let id = Number(_id);
        if (isNaN(id))
        {
            return undefined;
        }

        return {
            id: id,
            index: i,
        };
    }

    private static crt(n: Array<number>, a: Array<number>): number
    {
        let sum = 0;
        let prod = n.reduce((i: number, j: number) => i * j);

        for (let i = 0; i < n.length; i++)
        {
            let ni = n[i];
            let ai = a[i];
            let p = prod / ni;
            sum += ai * Day13.modInv(p, ni) * p;
        }

        return sum % prod;
    }

    private static modInv(a: number, b: number): number
    {
        let b0 = b;
        let x0 = 0;
        let x1 = 1;

        if (b === 1)
        {
            return 1;
        }

        while (a > 1)
        {
            let q = Math.trunc(a / b);

            let tmp = a;
            a = b;
            b = tmp % b;

            tmp = x0;
            x0 = x1 - q * x0;
            x1 = tmp;
        }

        if (x1 < 0)
        {
            x1 += b0;
        }

        return x1;
    }
}


export default Day13;
