import DayX, { DayXSolution } from "./DayX";
import { counter } from "../utils/Utils";


type Food = {
    ingredients: Set<string>;
    allergens: Set<string>;
};


class Day21 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)`;
    }

    protected _parseInput(_input: string): any
    {
        let input = _input.splitLines()
                          .map(i => i.trim())
                          .filter(Boolean)
                          .map(i => i.replace(/\)$/, ""))
                          .map(i => i.split("(contains ", 2));

        let _tally = new Array<string>();
        let _foods = input.map(([i, a]) => {
            let ingredients = i.trim().split(" ");
            let allergens = a.trim().split(", ");

            _tally = _tally.concat(ingredients);
            return {
                ingredients: new Set<string>(ingredients),
                allergens: new Set<string>(allergens),
            };
        });

        let ingredientsAll = _foods.map(f => f.ingredients)
                                   .reduce((a, b) => a.union(b));
        let allergensAll = _foods.map(f => f.allergens)
                                 .reduce((a, b) => a.union(b));

        let foods = new Array<Food>();
        allergensAll.forEach((a: string) => {
            let ingredients = _foods.filter(f => f.allergens.has(a))
                                    .map(f => f.ingredients)
                                    .reduce((a, b) => a.intersection(b));

            foods.push({
                ingredients: ingredients,
                allergens: new Set<string>([a]),
            });
        });

        let ingredientsSet = foods.map(f => f.ingredients);

        let tally = counter(_tally);

        return {
            ingredientsSet: ingredientsSet,
            ingredientsAll: ingredientsAll,
            foods: foods,
            tally: tally,
        };
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;

        let { ingredientsSet, ingredientsAll, tally } = input;
        let clear = ingredientsSet.reduce((a: Set<string>, b: Set<string>) => {
            return a.difference(b)
        }, ingredientsAll);
        clear.forEach((i: string) => result += tally.getOrDefault(i, 0));

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = "";

        let { foods: _foods } = input;
        let foods = _foods.map((i: Food) => {
            return {
                ingredients: new Set(i.ingredients),
                allergens: new Set(i.allergens),
            };
        });

        let mapping = new Map<string, string>();
        while (foods.some((i: Food) => i.ingredients.size > 0))
        {
            foods.forEach(({ingredients: i, allergens: a}: Food) => {
                if (i.size !== 1)
                {
                    return;
                }

                mapping.set(a.values().next().value, i.values().next().value);
                foods.forEach((f: Food) => {
                   f.ingredients = f.ingredients.difference(i);
                });
            });
        }

        result = Array.from(mapping.keys())
                      .sort()
                      .map(i => mapping.getOrDefault(i, ""))
                      .filter(Boolean)
                      .join(",");

        return {
            result: result,
            auxilary: undefined,
        };
    }
}


export default Day21;
