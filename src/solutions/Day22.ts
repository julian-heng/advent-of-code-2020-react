import DayX, { DayXSolution } from "./DayX";


class Day22 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10`;
    }

    protected _parseInput(_input: string): any
    {
        let [_p1, _p2] = _input.splitLinesMultiple(2)
        let p1 = _p1.splitLines()
                    .map(i => i.trim())
                    .filter(Boolean)
                    .slice(1)
                    .map(Number)
                    .reverse();
        let p2 = _p2.splitLines()
                    .map(i => i.trim())
                    .filter(Boolean)
                    .slice(1)
                    .map(Number)
                    .reverse();

        return [p1, p2];
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;
        let [_p1, _p2] = input;
        let p1 = [..._p1];
        let p2 = [..._p2];

        while (p1.length > 0 && p2.length > 0)
        {
            let a = p1.pop();
            let b = p2.pop();

            if (a > b)
            {
                p1.unshift(a);
                p1.unshift(b);
            }
            else
            {
                p2.unshift(b);
                p2.unshift(a);
            }
        }

        let r = p1.length > 0 ? p1 : p2;
        result = r.map((n: number, i: number) => (i + 1) * n)
                  .reduce((a: number, b: number) => a + b);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;
        let [_p1, _p2] = input;
        let p1 = [..._p1];
        let p2 = [..._p2];

        [p1, p2] = Day22.solve2(p1, p2, new Set<string>());

        let r = p1.length > 0 ? p1 : p2;
        result = r.map((n: number, i: number) => (i + 1) * n)
                  .reduce((a: number, b: number) => a + b);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve2(
        p1: Array<number>,
        p2: Array<number>,
        seen: Set<string>
    ): Array<Array<number>>
    {
        while (p1.length > 0 && p2.length > 0)
        {
            let k = JSON.stringify([[...p1], [...p2]]);
            if (seen.has(k))
            {
                return [p1, []];
            }
            else
            {
                seen.add(k);
            }

            let a = p1.pop();
            let b = p2.pop();

            if (a === undefined || b === undefined)
            {
                return a === undefined ? [[], p2] : [p1, []];
            }

            let p1_won = false;
            if (p1.length >= a && p2.length >= b)
            {
                let _p1 = p1.slice(p1.length - a);
                let _p2 = p2.slice(p2.length - b);
                [_p1, _p2] = Day22.solve2(_p1, _p2, new Set<string>());
                p1_won = _p1.length > 0 && _p2.length === 0;
            }
            else
            {
                p1_won = a > b;
            }

            if (p1_won)
            {
                p1.unshift(a);
                p1.unshift(b);
            }
            else
            {
                p2.unshift(b);
                p2.unshift(a);
            }
        }

        return [p1, p2];
    }
}


export default Day22;
