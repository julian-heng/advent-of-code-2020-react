import DayX, { DayXSolution } from "./DayX";


class Day09 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        // Annoyingly, the example input on the site has a premble length of 5,
        // whereas the puzzle input is 25. Therefore you would need to enter
        // your own puzzle input.
        return "";
    }

    protected _parseInput(_input: string): any
    {
        return _input.splitLines()
                     .map(i => i.trim())
                     .filter(Boolean)
                     .map(Number);
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;

        for (let i = 0, j = 24; (j + 1) < input.length; i++, j++)
        {
            let n = input[j + 1];
            let a = input.slice(i, j + 1);
            if (Day09.solve1(a, n))
            {
                result = n;
                break;
            }
        }

        return {
            result: result,
            auxilary: result,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = Day09.solve2(input, auxilary);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve1(nums: Array<number>, target: number): boolean
    {
        for (let i = 0; (i - 1) < nums.length; i++)
        {
            for (let j = i + 1; j < nums.length; j++)
            {
                if ((nums[i] + nums[j]) === target)
                {
                    return false;
                }
            }
        }

        return true;
    }

    private static solve2(nums: Array<number>, target: number): number
    {
        for (let i = 0; (i - 1) < nums.length; i++)
        {
            for (let j = i + 1; j < nums.length; j++)
            {
                let a = nums.slice(i, j + 1);
                if (a.reduce((a: number, b: number) => a + b) === target)
                {
                    return Math.min(...a) + Math.max(...a);
                }
            }
        }

        return 0;
    }
}


export default Day09;
