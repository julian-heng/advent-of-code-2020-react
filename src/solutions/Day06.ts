import DayX, { DayXSolution } from "./DayX";


class Day06 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `abc

a
b
c

ab
ac

a
a
a
a

b`;
    }

    protected _parseInput(_input: string): any
    {
        return _input.splitLinesMultiple()
                     .map(i => i.splitLines())
                     .map(i => i.map(j => j.trim())
                                .filter(Boolean)
                                .map(j => new Set(j)));
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let reducer = (a: Set<string>, b: Set<string>) => a.union(b);
        let result = Day06.solve(input, reducer);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let reducer = (a: Set<string>, b: Set<string>) => a.intersection(b);
        let result = Day06.solve(input, reducer);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve(
        input: Array<Array<Set<string>>>,
        reducer: (a: Set<string>, b: Set<string>) => Set<string>
    ): number
    {
        let result = 0;
        input.map((i: Array<Set<string>>) => i.reduce(reducer))
             .forEach((i: Set<string>) => result += i.size)
        return result;
    }
}


export default Day06;
