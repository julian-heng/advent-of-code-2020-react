import DayX, { DayXSolution } from "./DayX";


class Day10 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `16
10
15
5
1
11
7
19
6
12
4`;
    }

    protected _parseInput(_input: string): any
    {
        let input = _input.splitLines()
                          .map(i => i.trim())
                          .filter(Boolean)
                          .map(Number);

        input.sort((a: number, b: number) => a - b);
        input = [0, ...input, input[input.length - 1] + 3];
        return input;
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;
        let deltas = new Map<number, number>();

        for (let i = 0, j = 1; j < input.length; i++, j++)
        {
            let diff = input[j] - input[i];
            deltas.set(diff, deltas.getOrDefault(diff, 0) + 1);
        }

        result = deltas.getOrDefault(1, 0) * deltas.getOrDefault(3, 0);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;
        let counts = new Map<number, number>([[0, 1]]);

        for (let i = 0, j = 1; j < input.length; i++, j++)
        {
            let a = input[j];
            counts.set(a, counts.getOrDefault(a - 3, 0)
                          + counts.getOrDefault(a - 2, 0)
                          + counts.getOrDefault(a - 1, 0));
        }

        result = counts.getOrDefault(input[input.length - 1], 0);

        return {
            result: result,
            auxilary: undefined,
        };
    }
}


export default Day10;
