import DayX, { DayXSolution } from "./DayX";


class Day03 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#`;
    }

    protected _parseInput(_input: string): any
    {
        return _input.splitLines()
                     .map(i => i.trim())
                     .filter(Boolean);
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = Day03.solve(input, 3, 1);
        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 1;
        result *= Day03.solve(input, 1, 1);
        result *= Day03.solve(input, 3, 1);
        result *= Day03.solve(input, 5, 1);
        result *= Day03.solve(input, 7, 1);
        result *= Day03.solve(input, 1, 2);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve(map: string[], dx: number, dy: number): number
    {
        let r = 0;
        let d = 0;
        let trees = 0;

        while (d < map.length)
        {
            trees += Number(map[d].charAt(r) === "#");
            r = (r + dx) % map[d].length;
            d += dy;
        }

        return trees;
    }
}


export default Day03;
