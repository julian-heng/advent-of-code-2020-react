import DayX, { DayXSolution } from "./DayX";


class Day25 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `5764801
17807724`;
    }

    protected _parseInput(_input: string): any
    {
        let [card, door] = _input.splitLines()
                                 .map(i => i.trim())
                                 .filter(Boolean)
                                 .map(Number);
        return { card: card, door: door };
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;

        let { card, door } = input;
        let cardLoop = Day25.getLoopSize(card);
        let doorLoop = Day25.getLoopSize(door);
        let cardEncrypt = Day25.getEncryptionKey(card, doorLoop);
        let doorEncrypt = Day25.getEncryptionKey(door, cardLoop);

        result = cardEncrypt === doorEncrypt ? cardEncrypt : -1;

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static getLoopSize(n: number): number
    {
        let v = 1;
        let s = 7;
        let loop = 0;

        while (v !== n)
        {
            v = (v * s) % 20201227;
            loop++;
        }

        return loop;
    }

    private static getEncryptionKey(s: number, l: number): number
    {
        let v = 1;

        for (let i = 0; i < l; i++)
        {
            v = (v * s) % 20201227;
        }

        return v;
    }
}


export default Day25;
