import DayX, { DayXSolution } from "./DayX";


class Day05 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `BFFFBBFRRR
FFFBBBFRRR
BBFFBBFRLL`;
    }

    protected _parseInput(_input: string): any
    {
        return _input.splitLines()
                     .map(i => i.trim())
                     .filter(Boolean)
                     .map(Day05.decode)
                     .map(i => parseInt(i, 2));
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = Math.max(...input);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let results = [...input];
        results.sort((a: number, b: number) => a - b);

        let delta = results.map((e: number, i: number) => {
            return results[i + 1] === undefined ? 1 : results[i + 1] - e;
        });

        let d = delta.next((i: number) => i !== 1);
        let result = d === undefined ? 0 : results[delta.indexOf(d)] + 1;

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static decode(encode: string): string
    {
        encode = encode.replaceAll(/F|L/g, "0");
        encode = encode.replaceAll(/B|R/g, "1");
        return encode;
    }
}


export default Day05;
