import DayX, { DayXSolution } from "./DayX";
import { range } from "../utils/Utils";


type Pixel = boolean;

type TileData = {
    id: number;
    data: Array<Array<Pixel>>;
    north: Array<number>;
    east: Array<number>;
    south: Array<number>;
    west: Array<number>;
};


type Tile = {
    id: number;
    data: Array<TileData>;
};


class Day20 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...`;
    }

    protected _parseInput(_input: string): any
    {
        let tiles = new Map<number, Tile>();

        let input = _input.splitLinesMultiple()
                          .map(i => i.trim())
                          .filter(Boolean)
                          .map(Day20.makeTile);

        input.forEach(t => {
            if (t !== undefined)
            {
                tiles.set(t.id, t);
            }
        });

        return tiles;
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let grid = Day20.solve1(input);
        let result = grid[0][0].id;
        result *= grid[0][grid[0].length - 1].id;
        result *= grid[grid.length - 1][0].id;
        result *= grid[grid.length - 1][grid.length - 1].id;

        return {
            result: result,
            auxilary: grid,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let grid = auxilary;
        let result = Day20.solve2(grid);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve1(tiles: Map<number, Tile>): Array<Array<TileData>>
    {
        let n = Math.trunc(Math.sqrt(tiles.size));
        let grid = new Array<Array<TileData>>();
        let available = new Set<number>(tiles.keys());
        range(0, n).forEach(() => grid.push(new Array<TileData>(n)));

        Day20._solve1(tiles, available, grid, 0, 0);

        return grid;
    }

    private static _solve1(
        tiles: Map<number, Tile>,
        available: Set<number>,
        grid: Array<Array<TileData>>,
        x: number,
        y: number
    ): boolean
    {
        if (available.size === 0)
        {
            return true;
        }

        if (y >= grid.length || x >= grid[0].length)
        {
            return false;
        }

        let ids = Array.from(available);
        ids.sort((a: number, b: number) => a - b);

        for (let tileId of ids)
        {
            let tile = tiles.get(tileId);
            if (tile === undefined)
            {
                continue;
            }

            available.delete(tileId);

            for (let t of tile.data)
            {
                if (y > 0 && !grid[y - 1][x].south.equals(t.north))
                {
                    continue;
                }

                if (x > 0 && !grid[y][x - 1].east.equals(t.west))
                {
                    continue;
                }

                grid[y][x] = t;

                let _x = x + 1;
                let _y = y;

                if (x === grid.length - 1)
                {
                    _x = 0;
                    _y = y + 1;
                }

                if (Day20._solve1(tiles, available, grid, _x, _y))
                {
                    return true;
                }
            }

            available.add(tileId);
        }

        return false;
    }

    private static solve2(_grid: Array<Array<TileData>>): number
    {
        let tmpGrid = new Array<Array<Array<Array<Pixel>>>>();
        let grid = new Array<Array<Pixel>>();

        _grid.forEach((row: Array<TileData>) => {
            tmpGrid.push(row.map(i => Day20.removeBorder(i.data)));
        });

        for (let y = 0; y < tmpGrid.length; y++)
        {
            for (let x = 0; x < tmpGrid[y][0].length; x++)
            {
                grid.push(new Array<Pixel>());
            }
        }

        let n = tmpGrid[0][0].length;
        for (let y = 0; y < grid.length; y++)
        {
            for (let x = 0; x < (n * tmpGrid[0].length); x++)
            {
                let row = Math.trunc(y / n);
                let col = Math.trunc(x / n);
                let tileRow = y % n;
                let tileCol = x % n;
                grid[y].push(tmpGrid[row][col][tileRow][tileCol]);
            }
        }

        let monsterStr = [
            "                  # ",
            "#    ##    ##    ###",
            " #  #  #  #  #  #   "
        ].join("\n");
        let monsters = Day20.makeTileData(monsterStr, false);

        let result = grid.flatMap(i => i)
                         .filter(Boolean)
                         .length;

        let monsterSize = monsters[0].data
                                     .flatMap(i => i)
                                     .filter(Boolean)
                                     .length;

        for (let monster of monsters)
        {
            let count = 0;

            let height = grid.length - monster.data.length + 1;
            for (let y = 0; y < height; y++)
            {
                let width = grid.length - monster.data[0].length + 1;
                for (let x = 0; x < width; x++)
                {
                    if (Day20.match(grid, x, y, monster.data))
                    {
                        count++;
                    }
                }
            }

            if (count > 0)
            {
                result -= count * monsterSize;
                break;
            }
        }

        return result;
    }

    private static match(
        grid: Array<Array<Pixel>>,
        x: number,
        y: number,
        monster: Array<Array<Pixel>>
    ): boolean
    {
        for (let py = 0; py < monster.length; py++)
        {
            for (let px = 0; px < monster[py].length; px++)
            {
                if (!monster[py][px])
                {
                    continue;
                }

                if (!grid[y + py][x + px])
                {
                    return false;
                }
            }
        }

        return true;
    }

    private static makeTile(t: string): Tile | undefined
    {
        let m = t.match(/^Tile (\d+):\n([.#\n]*)$/);
        if (m === null)
        {
            return undefined;
        }

        let [, idStr, tileStr] = m;
        let id = Number(idStr);
        let data = Day20.makeTileData(tileStr, true);
        data.forEach(d => d.id = id);

        return {
            id: id,
            data: data,
        };
    }

    private static makeTileData(t: string, border: boolean = false):
        Array<TileData>
    {
        let tileTemplate = (data: Array<Array<Pixel>>) => {
            return {
                id: -1,
                data: data,
                north: new Array<number>(),
                east: new Array<number>(),
                south: new Array<number>(),
                west: new Array<number>(),
            };
        };

        let data = new Array<TileData>();
        let main = tileTemplate(t.splitLines()
                                 .map(i => Array.from(i))
                                 .map(i => i.map(j => j === "#")));
        data.push(main);

        let last = main;
        range(0, 3).forEach(() => {
            let transformed = tileTemplate(Day20.rotateTile(last.data));
            data.push(transformed);
            last = transformed;
        });

        last = tileTemplate(Day20.flipTile(main.data));
        data.push(last);

        range(0, 3).forEach(() => {
            let transformed = tileTemplate(Day20.rotateTile(last.data));
            data.push(transformed);
            last = transformed;
        });

        if (border)
        {
            data.forEach(Day20.calculateBorder);
        }

        return data;
    }

    private static rotateTile(px: Array<Array<Pixel>>): Array<Array<Pixel>>
    {
        let result = new Array<Array<Pixel>>();

        for (let j = 0; j < px[0].length; j++)
        {
            result.push(new Array<Pixel>());
        }

        for (let i = px.length - 1; i >= 0; i--)
        {
            for (let j = 0; j < px[i].length; j++)
            {
                result[j].push(px[i][j]);
            }
        }

        return result;
    }

    private static flipTile(px: Array<Array<Pixel>>): Array<Array<Pixel>>
    {
        let result = new Array<Array<Pixel>>();

        for (let j = 0; j < px[0].length; j++)
        {
            result.push(new Array<Pixel>());
        }

        for (let i = 0; i < px.length; i++)
        {
            for (let j = 0; j < px[i].length; j++)
            {
                result[j].push(px[i][j]);
            }
        }

        return result;
    }

    private static calculateBorder(tile: TileData): void
    {
        let north = tile.data[0];
        let east = new Array<Pixel>();
        let south = tile.data[tile.data.length - 1];
        let west = new Array<Pixel>();

        tile.data.forEach(row => {
            west.push(row[0]);
            east.push(row[row.length - 1]);
        })

        let indexOfPixel = (e: Pixel, i: number) => e ? i : -1;
        let filter = (i: number) => i >= 0;
        tile.north = north.map(indexOfPixel).filter(filter);
        tile.east = east.map(indexOfPixel).filter(filter);
        tile.south = south.map(indexOfPixel).filter(filter);
        tile.west = west.map(indexOfPixel).filter(filter);
    }

    private static removeBorder(px: Array<Array<Pixel>>): Array<Array<Pixel>>
    {
        return px.slice(1, px.length - 1).map(i => i.slice(1, i.length - 1));
    }
}


export default Day20;
