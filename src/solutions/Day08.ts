import DayX, { DayXSolution } from "./DayX";


const OPCODE_VALID = [
    "acc", "jmp", "nop"
] as const;

type OpCode = typeof OPCODE_VALID[number];
type Instruction = {
    op: OpCode;
    val: number;
};


type Day08Solution = {
    acc: number;
    loop: boolean;
};


class Day08 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`;
    }

    protected _parseInput(_input: string): any
    {
        let input = _input.splitLines()
                          .map(i => i.trim())
                          .filter(Boolean);

        return input.map(i => i.split(/\s+/, 2))
                    .map((i: Array<string>) => {
                        let [op, val] = i;
                        return Day08.makeInstruction(op as OpCode, val);
                    })
                    .filter(Boolean);
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let { acc: result } = Day08.solve(input);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;
        let valid = new Set(["jmp", "nop"]);
        let nopjmp = new Array<number>();
        input.forEach((e: Instruction, i: number) => {
            if (valid.has(e.op))
            {
                nopjmp.push(i);
            }
        });

        for (let i of nopjmp)
        {
            let modified = input.map((i: Instruction) => Object.assign({}, i));

            if (modified[i].op === "nop")
            {
                modified[i].op = "jmp";
            }
            else
            {
                modified[i].op = "nop";
            }

            let { acc, loop } = Day08.solve(modified);

            if (!loop)
            {
                result = acc;
                break;
            }
        }

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static makeInstruction(op: OpCode, valStr: string):
        Instruction | undefined
    {
        if (!(new Set(OPCODE_VALID).has(op)))
        {
            return undefined;
        }

        let val = Number(valStr);
        if (isNaN(val))
        {
            return undefined;
        }

        return {
            op: op,
            val: val,
        };
    }

    private static solve(instructions: Array<Instruction>): Day08Solution
    {
        let seen = new Set<number>();
        let acc = 0;
        let ip = 0;

        while (true)
        {
            if (ip >= instructions.length)
            {
                return {
                    acc: acc,
                    loop: false,
                };
            }

            if (seen.has(ip))
            {
                return {
                    acc: acc,
                    loop: true,
                };
            }

            seen.add(ip);
            let { op, val } = instructions[ip];

            switch (op)
            {
                case "acc":
                    acc += val;
                    ip++;
                    break;

                case "nop":
                    ip++;
                    break;

                case "jmp":
                    ip += val;
                    break;
            }
        }
    }
}


export default Day08;
