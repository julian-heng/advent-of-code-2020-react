import DayX, { DayXSolution } from "./DayX";
import { cartesian, range } from "../utils/Utils";


type Coordinate = {
    x: number;
    y: number;
};


class Day17 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `.#.
..#
###`;
    }

    protected _parseInput(_input: string): any
    {
        let input = _input.splitLines()
                          .map(i => i.trim())
                          .filter(Boolean)
                          .map(i => Array.from(i));
        let coords = new Array<Coordinate>();

        input.forEach((r: Array<string>, y: number) => {
            r.forEach((c: string, x: number) => {
                if (c === "#")
                {
                    coords.push({
                        x: x,
                        y: y,
                    });
                }
            });
        });

        return coords;
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = Day17.solve(input, 3);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = Day17.solve(input, 4);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve(start: Array<Coordinate>, ndim: number): number
    {
        let _coords = start.map(({x, y}) => {
            return [x, y].concat(Array(ndim - 2).fill(0));
        });

        let coords = new Set<string>(_coords.map(i => JSON.stringify(i)));

        let min = Number.MAX_SAFE_INTEGER;
        let max = Number.MIN_SAFE_INTEGER;

        _coords.transpose().forEach(c => {
            min = Math.min(Math.min(...c) - 1, min);
            max = Math.max(Math.max(...c) + 1, max);
        });

        let activeSet = new Set([3, 4]);

        for (let r = 0; r < 6; r++)
        {
            let next = new Array<Array<number>>();
            let _range = range(min, max + 1);
            for (let _p of cartesian<number>(...Array(ndim).fill(_range)))
            {
                let p = JSON.stringify(_p);
                let adjRange = _p.map((a: number) => range(a - 1, a + 2));
                let _adj = cartesian<number>(...adjRange);
                let adj = new Set<string>(_adj.map(i => JSON.stringify(i)));
                let numActive = coords.intersection(adj).size;

                if ((coords.has(p) && activeSet.has(numActive)) ||
                    numActive === 3)
                {
                    min = Math.min(Math.min(..._p) - 1, min);
                    max = Math.max(Math.max(..._p) + 1, max);
                    next.push(_p);
                }
            }

            coords = new Set<string>(next.map(i => JSON.stringify(i)));
        }

        return coords.size;
    }
}


export default Day17;
