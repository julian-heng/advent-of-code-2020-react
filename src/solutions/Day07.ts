import DayX, { DayXSolution } from "./DayX";


type Bag = {
    subBags: Map<string, number>;
};


const BAG_EMPTY: Bag = {
    subBags: new Map<string, number>(),
};


class Day07 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.`;
    }

    protected _parseInput(_input: string): any
    {
        let input = _input.splitLines()
                          .map(i => i.trim())
                          .filter(Boolean);

        let bags = new Map<string, Bag>();
        let re = /(\d+)\s(\w+\s\w+)/g;
        input.forEach(i => {
            let [bag, subBagsStr] = i.split(" bags contain ", 2);
            let subBags = new Map<string, number>();

            Array.from(subBagsStr.matchAll(re)).forEach(j => {
                let [, count, type] = j;
                subBags.set(type, Number(count));
            });

            bags.set(bag, {
                subBags: subBags,
            });
        });

        return bags;
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;
        let seen = new Map<string, boolean>();
        input.forEach((_: Bag, k: string) => {
            if (Day07.traversePart1(input, k, seen))
            {
                result++;
            }
        });

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let seen = new Map<string, number>();
        let result = Day07.traversePart2(input, "shiny gold", seen);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static traversePart1(
        bags: Map<string, Bag>,
        bag: string,
        seen: Map<string, boolean>
    ): boolean
    {
        if (seen.has(bag))
        {
            return seen.getOrDefault(bag, false);
        }

        let subBags = bags.getOrDefault(bag, BAG_EMPTY).subBags;
        if (subBags.has("shiny gold"))
        {
            seen.set(bag, true);
            return true;
        }

        for (let i of Array.from(subBags.keys()))
        {
            if (Day07.traversePart1(bags, i, seen))
            {
                seen.set(i, true);
                return true;
            }
        }

        seen.set(bag, false);
        return false;
    }

    private static traversePart2(
        bags: Map<string, Bag>,
        bag: string,
        seen: Map<string, number>
    ): number
    {
        if (seen.has(bag))
        {
            return seen.getOrDefault(bag, 0);
        }

        let result = 0;
        let subBags = bags.getOrDefault(bag, BAG_EMPTY).subBags;
        subBags.forEach((v: number, k: string) => {
            result += v + (v * Day07.traversePart2(bags, k, seen));
        });

        seen.set(bag, result);
        return result;
    }
}


export default Day07;
