import DayX, { DayXSolution } from "./DayX";
import { range } from "../utils/Utils";


class Day23 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `389125467`;
    }

    protected _parseInput(_input: string): any
    {
        let m = _input.match(/\d+/);
        if (m === null)
        {
            return undefined;
        }

        return Array.from(m.values().next().value).map(Number);
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = Day23.solve([...input], 100, 1);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = Day23.solve([...input], 10_000_000, 2);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve(nums: Array<number>, limit: number, part: number):
        number
    {
        let min = Math.min(...nums);
        let max = Math.max(...nums);
        let current = nums[0];

        if (part === 2)
        {
            nums = nums.concat(range(max + 1, 1_000_000 + 1))
            max = nums[nums.length - 1];
        }

        let cups = new Map<number, number>();
        for (let i = 0; i < nums.length; i++)
        {
            cups.set(nums[i], nums[(i + 1) % nums.length]);
        }

        for (let i = 0; i < limit; i++)
        {
            let pickUp = new Array<number>();
            let next = current;

            for (let j = 0; j < 3; j++)
            {
                pickUp.push(cups.getOrDefault(next, 0));
                next = cups.getOrDefault(next, 0);
            }

            cups.set(current, cups.getOrDefault(next, 0));
            let dest = current - 1;
            let pickUpSet = new Set(pickUp);
            while (!cups.has(dest) || pickUpSet.has(dest))
            {
                dest--;
                if (dest < min)
                {
                    dest = max;
                }
            }

            let tmp = cups.getOrDefault(dest, 0);
            cups.set(dest, pickUp[0]);
            cups.set(pickUp[pickUp.length - 1], tmp)
            current = cups.getOrDefault(current, 0);
        }

        let result = 0;
        if (part === 1)
        {
            let start = 1;
            let next = cups.getOrDefault(start, 0);
            while (next !== start)
            {
                result = (result * 10) + next;
                next = cups.getOrDefault(next, 0);
            }
        }
        else
        {
            console.log(cups);
            let n = cups.getOrDefault(1, 0);
            result = n * cups.getOrDefault(n, 0);
        }

        return result;
    }
}


export default Day23;
