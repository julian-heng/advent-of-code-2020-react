import Day01 from "./Day01";
import Day02 from "./Day02";
import Day03 from "./Day03";
import Day04 from "./Day04";
import Day05 from "./Day05";
import Day06 from "./Day06";
import Day07 from "./Day07";
import Day08 from "./Day08";
import Day09 from "./Day09";
import Day10 from "./Day10";
import Day11 from "./Day11";
import Day12 from "./Day12";
import Day13 from "./Day13";
import Day14 from "./Day14";
import Day15 from "./Day15";
import Day16 from "./Day16";
import Day17 from "./Day17";
import Day18 from "./Day18";
import Day19 from "./Day19";
import Day20 from "./Day20";
import Day21 from "./Day21";
import Day22 from "./Day22";
import Day23 from "./Day23";
import Day24 from "./Day24";
import Day25 from "./Day25";
import DayX from "./DayX";


export function getSolution(n: number): typeof DayX | undefined
{
    switch (n)
    {
    case 1: return Day01;
    case 2: return Day02;
    case 3: return Day03;
    case 4: return Day04;
    case 5: return Day05;
    case 6: return Day06;
    case 7: return Day07;
    case 8: return Day08;
    case 9: return Day09;
    case 10: return Day10;
    case 11: return Day11;
    case 12: return Day12;
    case 13: return Day13;
    case 14: return Day14;
    case 15: return Day15;
    case 16: return Day16;
    case 17: return Day17;
    case 18: return Day18;
    case 19: return Day19;
    case 20: return Day20;
    case 21: return Day21;
    case 22: return Day22;
    case 23: return Day23;
    case 24: return Day24;
    case 25: return Day25;
    default: return undefined;
    }
}


export function getSolutionBackend(n: number): () => DayX | undefined
{
    switch (n)
    {
    case 1: return () => new Day01();
    case 2: return () => new Day02();
    case 3: return () => new Day03();
    case 4: return () => new Day04();
    case 5: return () => new Day05();
    case 6: return () => new Day06();
    case 7: return () => new Day07();
    case 8: return () => new Day08();
    case 9: return () => new Day09();
    case 10: return () => new Day10();
    case 11: return () => new Day11();
    case 12: return () => new Day12();
    case 13: return () => new Day13();
    case 14: return () => new Day14();
    case 15: return () => new Day15();
    case 16: return () => new Day16();
    case 17: return () => new Day17();
    case 18: return () => new Day18();
    case 19: return () => new Day19();
    case 20: return () => new Day20();
    case 21: return () => new Day21();
    case 22: return () => new Day22();
    case 23: return () => new Day23();
    case 24: return () => new Day24();
    case 25: return () => new Day25();
    default: return () => undefined;
    }
}


export function getInput(n: number): string
{
    let solution = getSolution(n);
    if (solution === undefined)
    {
        return "";
    }
    return solution.EXAMPLE_INPUT;
}


export { default as Day01 } from "./Day01";
export { default as Day02 } from "./Day02";
export { default as Day03 } from "./Day03";
export { default as Day04 } from "./Day04";
export { default as Day05 } from "./Day05";
export { default as Day06 } from "./Day06";
export { default as Day07 } from "./Day07";
export { default as Day08 } from "./Day08";
export { default as Day09 } from "./Day09";
export { default as Day10 } from "./Day10";
export { default as Day11 } from "./Day11";
export { default as Day12 } from "./Day12";
export { default as Day13 } from "./Day13";
export { default as Day14 } from "./Day14";
export { default as Day15 } from "./Day15";
export { default as Day16 } from "./Day16";
export { default as Day17 } from "./Day17";
export { default as Day18 } from "./Day18";
export { default as Day19 } from "./Day19";
export { default as Day20 } from "./Day20";
export { default as Day21 } from "./Day21";
export { default as Day22 } from "./Day22";
export { default as Day23 } from "./Day23";
export { default as Day24 } from "./Day24";
export { default as Day25 } from "./Day25";
export { default as DayX } from "./DayX";
