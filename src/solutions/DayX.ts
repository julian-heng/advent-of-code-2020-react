export class DayXState
{
    rawInput: string = "";
    input: any = undefined;
    part1Result: string | number = 0;
    part2Result: string | number = 0;
    auxilary: any = undefined;
}


export type DayXSolution = {
    result: string | number;
    auxilary: any;
}


export default abstract class DayX
{
    public createState(): DayXState
    {
        return new DayXState();
    }

    public parseInput(state: DayXState): DayXState
    {
        let rawInput = state.rawInput;
        let input = this._parseInput(rawInput);
        let nextState = Object.assign({}, state, {
            input: input,
        });
        return nextState;
    }

    public solvePart1(state: DayXState): DayXState
    {
        let input = state.input;
        let auxilary = state.auxilary;
        let solution = this._solvePart1(input, auxilary);
        let nextState = Object.assign({}, state, {
            part1Result: solution.result,
            auxilary: solution.auxilary,
        });
        return nextState;
    }

    public solvePart2(state: DayXState): DayXState
    {
        let input = state.input;
        let auxilary = state.auxilary;
        let solution = this._solvePart2(input, auxilary);
        let nextState = Object.assign({}, state, {
            part2Result: solution.result,
            auxilary: solution.auxilary,
        });
        return nextState;
    }

    public static get EXAMPLE_INPUT(): string
    {
        return "Child classes should override this get method";
    }

    protected abstract _parseInput(_input: string): any;

    protected abstract _solvePart1(
        input: any,
        auxilary: any | undefined
    ): DayXSolution;

    protected abstract _solvePart2(
        input: any,
        auxilary: any | undefined
    ): DayXSolution;
}
