import DayX, { DayXSolution } from "./DayX";


class Day18 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `1 + 2 * 3 + 4 * 5 + 6
2 * 3 + (4 * 5)
5 + (8 * 3 + 9 + 3 * 4 * 3)
5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))
((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2`;
    }

    protected _parseInput(_input: string): any
    {
        return _input.splitLines()
                     .map(i => i.trim())
                     .filter(Boolean)
                     .map(Day18.tokenize);
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = input.map((i: Array<string>) => Day18.solve(i, 1))
                          .reduce((a: number, b: number) => a + b);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = input.map((i: Array<string>) => Day18.solve(i, 2))
                          .reduce((a: number, b: number) => a + b);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve(tokens: Array<string>, version: number = 1): number
    {
        let result: number | undefined = undefined;
        let action = "";

        for (let i = 0; i < tokens.length; i++)
        {
            let t = tokens[i];
            let n = 0;
            let _t = Array.from(t);

            if (_t.some(i => !isNaN(Number(i))))
            {
                if (t.startsWith("(") && t.endsWith(")"))
                {
                    let next = Day18.tokenize(t.slice(1, t.length - 1));
                    n = Day18.solve(next, version);
                }
                else
                {
                    n = Number(t);
                }

                if (result === undefined)
                {
                    result = n;
                }
                else
                {
                    switch (action)
                    {
                    case "+":
                        result += n;
                        break;

                    case "-":
                        result -= n;
                        break;

                    case "*":
                        result *= n;
                        break;
                    }
                }
            }
            else
            {
                if (result !== undefined && version === 2 && t === "*")
                {
                    let next = tokens.slice(i + 1, tokens.length);
                    result *= Day18.solve(next, version);
                    return result;
                }

                action = t;
            }
        }

        return result !== undefined ? result : 0;
    }

    private static tokenize(eq: string): Array<string>
    {
        let opSet = new Set(["+", "-", "*"]);
        let tokens = new Array<string>();
        let token = "";
        let state = 1;
        let level = 0;

        let it = eq[Symbol.iterator]();
        // eslint-disable-next-line
        for (let t = null; t = it.next().value; )
        {
            if (t === " ")
            {
                continue;
            }
            else if (t === "(")
            {
                level++;
                if (state === 1)
                {
                    state = 0;
                    if (token !== "")
                    {
                        tokens.push(token);
                    }
                    token = "";
                }
                token += t;
            }
            else if (t === ")")
            {
                level--;
                token += t;
                if (level === 0)
                {
                    state = 1;
                    if (token !== "")
                    {
                        tokens.push(token);
                    }
                    token = "";
                }
            }
            else if (opSet.has(t) && state !== 0)
            {
                if (token !== "")
                {
                    tokens.push(token);
                }
                token = "";
                tokens.push(t);
            }
            else
            {
                token += t;
            }
        }

        if (token !== "")
        {
            tokens.push(token);
        }

        return tokens;
    }
}


export default Day18;
