import DayX, { DayXSolution } from "./DayX";
import { cartesian, range } from "../utils/Utils";


class Day24 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew`;
    }

    protected _parseInput(_input: string): any
    {
        return _input.splitLines()
                     .map(i => i.trim())
                     .filter(Boolean);
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;

        let tiles = new Set<string>();
        let re = /(?:(?:s|n)?(?:e|w))/g;
        input.forEach((i: string) => {
            let c = 0;
            let r = 0;
            let it = i.matchAll(re);

            // eslint-disable-next-line
            for (let d = null; d = it.next().value; )
            {
                let [dc, dr] = Day24.calculateDelta(d[0], c, r);
                c += dc;
                r += dr;
            }

            let coords = JSON.stringify([c, r]);

            if (tiles.has(coords))
            {
                tiles.delete(coords);
            }
            else
            {
                tiles.add(coords);
            }
        });

        result = tiles.size;

        return {
            result: result,
            auxilary: tiles,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;

        let tiles = auxilary;
        let _tiles: Array<Array<number>>;
        _tiles = Array.from(tiles.map(JSON.parse));

        let min = Number.MAX_SAFE_INTEGER;
        let max = Number.MIN_SAFE_INTEGER;

        _tiles.transpose().forEach((t: Array<number>) => {
            min = Math.min(Math.min(...t) - 1, min);
            max = Math.max(Math.max(...t) + 1, max);
        });

        let activeSet = new Set([2, 3]);

        for (let r = 0; r < 100; r++)
        {
            let next = new Array<Array<number>>();
            let _range = range(min, max + 1);
            for (let _p of cartesian<number>(...Array(2).fill(_range)))
            {
                let p = JSON.stringify(_p);
                let _adj = Day24.getAdjacent(_p[0], _p[1]);
                let adj = new Set<string>(_adj.map(i => JSON.stringify(i)));
                let numActive = tiles.intersection(adj).size;

                if ((tiles.has(p) && activeSet.has(numActive)) ||
                    numActive === 2)
                {
                    min = Math.min(Math.min(..._p) - 1, min);
                    max = Math.max(Math.max(..._p) + 1, max);
                    next.push(_p);
                }
            }

            tiles = new Set<string>(next.map(i => JSON.stringify(i)));
        }

        result = tiles.size;

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static calculateDelta(d: string, c: number, r: number):
        Array<number>
    {
        let dc = 0;
        let dr = 0;

        switch (d)
        {
            case "":
                return [dc, dr];

            case "w":
                dc = -1;
                break;

            case "e":
                dc = 1;
                break;

            default:
                let offset = Number((r % 2) !== 0);
                if (d.includes("s"))
                {
                    dr = 1;
                }
                else if (d.includes("n"))
                {
                    dr = -1;
                }

                if (d.includes("w"))
                {
                    dc = offset ? 0 : -1;
                }
                else if (d.includes("e"))
                {
                    dc = offset ? 1 : 0;
                }
        }

        return [dc, dr];
    }

    private static getAdjacent(c: number, r: number): Array<Array<number>>
    {
        let adj = new Array<Array<number>>();
        let dirs = ["", "e", "se", "sw", "w", "nw", "ne"];

        dirs.forEach(d => {
            let [dc, dr] = Day24.calculateDelta(d, c, r);
            adj.push([c + dc, r + dr]);
        });

        return adj;
    }
}


export default Day24;
