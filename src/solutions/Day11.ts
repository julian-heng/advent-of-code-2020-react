import DayX, { DayXSolution } from "./DayX";
import { inBounds } from "../utils/Utils";


type Coordinate = {
    y: number,
    x: number,
};

type Seat = "." | "L" | "#";
const SEAT_FLOOR: Seat = ".";
const SEAT_EMPTY: Seat = "L";
const SEAT_TAKEN: Seat = "#";

const seatIsFloor = (seat: Seat) => seat === SEAT_FLOOR;
const seatIsEmpty = (seat: Seat) => seat === SEAT_EMPTY;
const seatIsTaken = (seat: Seat) => seat === SEAT_TAKEN;


class Day11 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL`;
    }

    protected _parseInput(_input: string): any
    {
        return _input.splitLines()
                     .map(i => i.trim())
                     .filter(Boolean)
                     .map(i => Array.from(i)
                                    .map(Day11.parseSeat));
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let copy = input.map((i: Array<Seat>) => [...i]);
        let result = Day11.solve(copy, Day11.adjacent_at, 4);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let copy = input.map((i: Array<Seat>) => [...i]);
        let result = Day11.solve(copy, Day11.adjacent_all, 5);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve(
        seats: Array<Array<Seat>>,
        adjCallback: (
            grid: Array<Array<Seat>>,
            row: number,
            col: number
        ) => Array<Seat>,
        limit: number
    ): number
    {
        let changes: Array<Coordinate>;

        do
        {
            changes = new Array<Coordinate>();

            // eslint-disable-next-line
            seats.forEach((row, r) => row.forEach((seat, c) => {
                if (seatIsFloor(seat))
                {
                    return;
                }

                let taken = adjCallback(seats, r, c).filter(seatIsTaken);
                if ((seatIsEmpty(seat) && taken.length === 0) ||
                    (seatIsTaken(seat) && taken.length > limit))
                {
                    changes.push({
                        y: r,
                        x: c,
                    });
                }
            }));

            for (let { y: r, x: c } of changes)
            {
                if (seatIsTaken(seats[r][c]))
                {
                    seats[r][c] = SEAT_EMPTY;
                }
                else
                {
                    seats[r][c] = SEAT_TAKEN;
                }
            }
        } while (changes.length > 0);

        return seats.map((i: Array<Seat>) => i.filter(seatIsTaken))
                    .map((i: Array<Seat>) => i.length)
                    .reduce((a: number, b: number) => a + b);
    }

    private static adjacent_at(
        grid: Array<Array<Seat>>,
        row: number,
        col: number
    ): Array<Seat>
    {
        let rmin = Math.max(row - 1, 0);
        let rmax = Math.min(row + 2, grid.length);
        let cmin = Math.max(col - 1, 0);
        let cmax = Math.min(col + 2, grid[row].length);
        let results = new Array<Seat>();

        for (let r = rmin; r < rmax; r++)
        {
            for (let c = cmin; c < cmax; c++)
            {
                results.push(grid[r][c]);
            }
        }

        return results;
    }

    private static adjacent_all(
        grid: Array<Array<Seat>>,
        row: number,
        col: number
    ): Array<Seat>
    {
        let directions: Array<Coordinate> = [
            {y: -1, x: -1}, {y: -1, x:  0}, {y: -1, x:  1},
            {y:  0, x: -1},                 {y:  0, x:  1},
            {y:  1, x: -1}, {y:  1, x:  0}, {y:  1, x:  1},
        ];

        let results = new Array<Seat>(grid[row][col]);

        directions.forEach(({y, x}) => {
            results.push(Day11.raycast(grid, row, col, y, x));
        });

        return results;
    }

    private static parseSeat(i: string): Seat | undefined
    {
        switch (i)
        {
            case ".":
                return SEAT_FLOOR;

            case "L":
                return SEAT_EMPTY;

            case "#":
                return SEAT_TAKEN;
        }

        return undefined;
    }

    private static raycast(
        grid: Array<Array<Seat>>,
        row: number,
        col: number,
        dy: number,
        dx: number
    ): Seat
    {
        row += dy;
        col += dx;

        while (inBounds(grid, row, col))
        {
            if (!seatIsFloor(grid[row][col]))
            {
                return grid[row][col];
            }

            row += dy;
            col += dx;
        }

        return SEAT_EMPTY;
    }
}


export default Day11;
