import DayX, { DayXSolution } from "./DayX";


class Day16 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        // Day 16 sample input doesn't have the departure ranges
        return "";
    }

    protected _parseInput(_input: string): any
    {
        let [rulesStr, ticketStr, nearbyStr] = (
            _input.splitLinesMultiple()
                  .map(i => i.splitLines()
                             .map(j => j.trim())
                             .filter(Boolean))
        );

        ticketStr.shift();
        nearbyStr.shift();

        let rules = new Map<string, Set<number>>();
        rulesStr.forEach(i => {
            let [name, rangesStr] = i.split(":", 2);
            let ranges = new Set<number>();

            let it = rangesStr.matchAll(/(\d+)-(\d+)/g);
            // eslint-disable-next-line
            for (let m = null; m = it.next().value; )
            {
                let [, lowStr, highStr] = m;
                let low = Number(lowStr);
                let high = Number(highStr);

                if (isNaN(low) || isNaN(high))
                {
                    continue;
                }

                for (let n = low; n <= high; n++)
                {
                    ranges.add(n);
                }
            }

            rules.set(name, ranges);
        });

        let ticket = ticketStr[0].split(",")
                                 .map(Number)
                                 .filter(i => !isNaN(i));

        let nearby = nearbyStr.map(i => i.split(",")
                                         .map(Number)
                                         .filter(i => !isNaN(i)));

        return [rules, ticket, nearby];
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;
        let [rules,, nearby] = input;

        let r = new Set<number>();
        for (let a of rules.values())
        {
            r = r.union(a);
        }

        let valid = new Array<Array<number>>();
        for (let t of nearby)
        {
            let filtered = t.filter((i: number) => !r.has(i));
            if (filtered.length > 0)
            {
                result += filtered.reduce((a: number, b: number) => a + b);
            }
            else
            {
                valid.push(t);
            }
        }

        return {
            result: result,
            auxilary: valid,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = 0;

        let [rules, ticket,] = input;
        let valid = auxilary;

        let columnChoices = new Array<Array<string>>()
        for (let col of valid.transpose())
        {
            let choices = new Array<string>();
            for (let [k, v] of rules)
            {
                if (col.every((i: number) => v.has(i)))
                {
                    choices.push(k);
                }
            }
            columnChoices.push(choices);
        }

        let departure = new Array<number>();
        while (columnChoices.some(i => i.length > 0))
        {
            for (let i = 0; i < columnChoices.length; i++)
            {
                let col = columnChoices[i];
                if (col.length !== 1)
                {
                    continue;
                }

                let choice = col[0];
                columnChoices = columnChoices.map(i => {
                    return i.filter(j => j !== choice);
                })

                if (choice.startsWith("departure"))
                {
                    departure.push(i);
                }
            }
        }

        result = departure.map(i => ticket[i])
                          .reduce((a: number, b: number) => a * b);

        return {
            result: result,
            auxilary: undefined,
        };
    }
}


export default Day16;
