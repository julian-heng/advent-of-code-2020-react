import DayX, { DayXSolution } from "./DayX";


type Password = {
    min: number;
    max: number;
    target: string;
    password: string;
};


class Day02 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc`;
    }

    protected _parseInput(_input: string): any
    {
        let re = /(\d+)-(\d+)\s(\w):\s(.*)/g;
        return Array.from(_input.matchAll(re)).map(i => {
            return {
                min: Number(i[1]),
                max: Number(i[2]),
                target: i[3],
                password: i[4],
            }
        });
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = input.filter((i: Password) => {
            let count = i.password.charCount(i.target);
            return i.min <= count && count <= i.max;
        }).length;

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = input.filter((i: Password) => {
            let a = i.password.charAt(i.min - 1) === i.target;
            let b = i.password.charAt(i.max - 1) === i.target;
            return a !== b;
        }).length;

        return {
            result: result,
            auxilary: undefined,
        };
    }
}


export default Day02;
