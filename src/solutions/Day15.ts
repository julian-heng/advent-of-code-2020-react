import DayX, { DayXSolution } from "./DayX";


class Day15 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        return `0,3,6`;
    }

    protected _parseInput(_input: string): any
    {
        return _input.splitLines()
                     .map(i => i.trim())
                     .filter(Boolean)
                     .map(i => i.split(",").map(Number))
                     .flat();
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = Day15.solve(input, 2020);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = Day15.solve(input, 30000000);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve(nums: Array<number>, target: number): number
    {
        let spoken = new Map<number, number>();
        let num = 0;

        for (let t = 0; t < target - 1; t++)
        {
            if (t < nums.length - 1)
            {
                num = nums[t];
                spoken.set(num, t);
                continue;
            }
            else if (t === (nums.length - 1))
            {
                num = nums[nums.length - 1];
            }

            if (!spoken.has(num))
            {
                spoken.set(num, t);
                num = 0;
            }
            else
            {
                let tmp = num;
                num = t - spoken.getOrDefault(num, 0);
                spoken.set(tmp, t);
            }
        }

        return num;
    }
}


export default Day15;
