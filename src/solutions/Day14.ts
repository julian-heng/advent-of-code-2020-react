import DayX, { DayXSolution } from "./DayX";


const ACTION_VALID = [
    "mask", "write", "nop"
] as const;

type Action = typeof ACTION_VALID[number];

type Instruction = {
    action: Action;
    mask: string;
    address: number;
    value: number;
};


class Day14 extends DayX
{
    public static get EXAMPLE_INPUT(): string
    {
        // Day 14's example input is different for both parts
        return "";
    }

    protected _parseInput(_input: string): any
    {
        return _input.splitLines()
                     .map(i => i.trim())
                     .filter(Boolean)
                     .map(Day14.makeInstruction);
    }

    protected _solvePart1(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = Day14.solve(input, Day14.callback1);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    protected _solvePart2(input: any, auxilary: any | undefined = undefined):
        DayXSolution
    {
        let result = Day14.solve(input, Day14.callback2);

        return {
            result: result,
            auxilary: undefined,
        };
    }

    private static solve(
        instructions: Array<Instruction>,
        callback: (
            mem: Map<number, number>,
            mask: Array<string>,
            address: number,
            value: number
        ) => void
    ): number
    {
        let mem = new Map<number, number>();
        let mask = new Array<string>();

        for (let i of instructions)
        {
            switch (i.action)
            {
            case "mask":
                mask = Array.from(i.mask);
                break;

            case "write":
                let { address, value } = i;
                callback(mem, mask, address, value);
                break;
            }
        }

        return Array.from(mem.values()).reduce((i, j) => i + j, 0);
    }

    private static callback1(
        mem: Map<number, number>,
        mask: Array<string>,
        address: number,
        value: number
    ): void
    {
        mem.set(address, Day14.applyMask(mask, value));
    }

    private static callback2(
        mem: Map<number, number>,
        mask: Array<string>,
        address: number,
        value: number
    ): void
    {
        let floats = Day14.calculateFloats(mask, address);
        floats.forEach((f: number) => mem.set(f, value))
    }

    private static applyMask(mask: Array<string>, value: number): number
    {
        let bits = Array.from(value.toString(2).padStart(36, "0"));
        let resultStr = mask.map((e: string, i: number) => {
            return e === "X" ? bits[i] : e;
        });

        let result = parseInt(resultStr.join(""), 2);

        return result;
    }

    private static calculateFloats(mask: Array<string>, address: number):
        Array<number>
    {
        let bits = Array.from(address.toString(2).padStart(36, "0"));
        let [first, ...rest] = mask;
        let result = new Array<string>();

        if (first === "X")
        {
            result.push("0");
            result.push("1");
        }
        else
        {
            result.push(first);
        }

        rest.forEach((e: string, _i: number) => {
            let i = _i + 1;
            if (e === "X")
            {
                let rLength = result.length;
                for (let r = 0; r < rLength; r++)
                {
                    let v = result[r];
                    result[r] = v + "0";
                    result.push(v + "1");
                }
            }
            else
            {
                let a = e === "1" ? "1" : bits[i];
                for (let r = 0; r < result.length; r++)
                {
                    result[r] += a;
                }
            }
        });

        return result.map(i => Number(parseInt(i, 2)));
    }

    private static makeInstruction(i: string): Instruction
    {
        let action: Action = "nop";
        let mask = "";
        let address = 0;
        let value = 0;

        if (i.startsWith("mask"))
        {
            action = "mask";
            mask = i.slice(i.length - 36, i.length);
        }
        else
        {
            action = "write";
            let vals = Array.from(i.matchAll(/\d+/g));
            address = Number(vals[0][0]);
            value = Number(vals[1][0]);
        }

        return {
            action: action,
            mask: mask,
            address: address,
            value: value,
        };
    }
}


export default Day14;
