declare global
{
    interface Map<K, V>
    {
        getOrDefault(key: K, fallback: V): V;
    }
}


if (!Map.prototype.getOrDefault)
{
    // eslint-disable-next-line
    Map.prototype.getOrDefault = function<K, V>(key: K, fallback: V): V
    {
        let map = this;
        let value = map.get(key);
        return value === undefined ? fallback : value;
    }
}


export {};
