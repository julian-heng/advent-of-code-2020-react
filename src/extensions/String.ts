declare global
{
    interface String
    {
        charCount(target: string): number;
        splitLines(n?: number | undefined): Array<string>;
        splitLinesMultiple(n?: number | undefined): Array<string>;
    }
}


if (!String.prototype.charCount)
{
    // eslint-disable-next-line
    String.prototype.charCount = function(target: string): number
    {
        let str = this;
        if (target.length !== 1)
        {
            return 0;
        }

        let count = 0;

        let it = str[Symbol.iterator]();
        // eslint-disable-next-line
        for (let i = null; i = it.next().value; )
        {
            if (i === target)
            {
                count++;
            }
        }

        return count;
    };
}


if (!String.prototype.splitLines)
{
    // eslint-disable-next-line
    String.prototype.splitLines = function(n?: number | undefined):
        Array<string>
    {
        let s = this;
        return n !== undefined ? s.split("\n") : s.split("\n", n);
    };
}


if (!String.prototype.splitLinesMultiple)
{
    // eslint-disable-next-line
    String.prototype.splitLinesMultiple = function(n?: number | undefined):
        Array<string>
    {
        let s = this;
        return n !== undefined ? s.split("\n\n") : s.split("\n\n", n);
    };
}


export {};
