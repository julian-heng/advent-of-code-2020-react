declare global
{
    interface Set<T>
    {
        filter(f: (e: T) => boolean): Set<T>;
        map(f: (e: T) => T): Set<T>;

        intersection(s2: Set<T>): Set<T>;
        union(s2: Set<T>): Set<T>;
        difference(s2: Set<T>): Set<T>;
    }
}


if (!Set.prototype.filter)
{
    // eslint-disable-next-line
    Set.prototype.filter = function<T>(f: (e: T) => boolean): Set<T>
    {
        let set = this;
        let result = new Set<T>();

        let it = set[Symbol.iterator]();
        // eslint-disable-next-line
        for (let e = null; e = it.next().value; )
        {
            if (f(e))
            {
                result.add(e);
            }
        }

        return result;
    };
}


if (!Set.prototype.map)
{
    // eslint-disable-next-line
    Set.prototype.map = function<T>(f: (e: T) => T): Set<T>
    {
        let s = this;
        let result = new Set<T>();
        s.forEach(e => result.add(f(e)));
        return result;
    };
}


if (!Set.prototype.intersection)
{
    // eslint-disable-next-line
    Set.prototype.intersection = function<T>(s2: Set<T>): Set<T>
    {
        let s1 = this;
        return s1.filter(e => s2.has(e));
    };
}


if (!Set.prototype.union)
{
    // eslint-disable-next-line
    Set.prototype.union = function<T>(s2: Set<T>): Set<T>
    {
        let s1 = this;
        let result = new Set(s1);
        s2.forEach(e => result.add(e));
        return result;
    };
}


if (!Set.prototype.difference)
{
    // eslint-disable-next-line
    Set.prototype.difference = function<T>(s2: Set<T>): Set<T>
    {
        let s1 = this;
        let result = new Set(s1);
        result = result.filter(e => !s2.has(e));
        return result;
    };
}


export {};
