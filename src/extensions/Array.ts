declare global
{
    interface Array<T>
    {
        next(
            f: (e: T) => boolean,
            fallback?: T | undefined
        ): T | undefined;
        rotate(offset: number): void;
        transpose(): Array<T>;
        equals(b: Array<T>): boolean;
    }
}


if (!Array.prototype.next)
{
    // eslint-disable-next-line
    Array.prototype.next = (
        function<T>(f: (e: T) => boolean, fallback?: T | undefined):
            T | undefined
        {
            let arr = this;

            let it = arr[Symbol.iterator]();
            // eslint-disable-next-line
            for (let e = null; e = it.next().value;)
            {
                if (f(e))
                {
                    return e;
                }
            }

            return fallback;
        }
    );
}


if (!Array.prototype.rotate)
{
    // eslint-disable-next-line
    Array.prototype.rotate = function<T>(offset: number): void
    {
        let arr = this;

        offset %= arr.length;
        if (offset < 0)
        {
            offset = arr.length - Math.abs(offset);
        }

        arr.unshift(...arr.slice(arr.length - offset, arr.length));
    }
}


if (!Array.prototype.transpose)
{
    // eslint-disable-next-line
    Array.prototype.transpose = function<T>(): Array<T>
    {
        let arr = this;
        return arr[0].map((_: T, i: number) => arr.map(row => row[i]));
    }
}


if (!Array.prototype.equals)
{
    // eslint-disable-next-line
    Array.prototype.equals = function<T>(b: Array<T>): boolean
    {
        let a = this;

        if (!b || a.length !== b.length)
        {
            return false;
        }

        for (let i = 0; i < a.length; i++)
        {
            if (a[i] instanceof Array && b[i] instanceof Array)
            {
                if (!a[i].equals(b[i]))
                {
                    return false;
                }
            }
            else if (a[i] !== b[i])
            {
                return false;
            }
        }

        return true;
    };
}


export {};
