import React, { MouseEvent } from "react";

import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import DayX from "../solutions/DayX";


type SolutionComponentProps = {
    backend: () => DayX | undefined;
    placeholder: string;
};


type SolutionComponentState = {
    instance?: DayX;
    input: string;
    output: string;
    elapsedTime?: number;
}


const PLACEHOLDER_PLACEHOLDER = "Enter input here..."


class SolutionComponent extends React.Component<SolutionComponentProps,
                                                SolutionComponentState>
{
    constructor(props: SolutionComponentProps)
    {
        super(props);

        this.state = {
            instance: this.props.backend(),
            input: "",
            output: "",
            elapsedTime: undefined,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.reset = this.reset.bind(this);
    }

    render()
    {
        let placeholder = this.props.placeholder;
        if (placeholder === "")
        {
            placeholder = PLACEHOLDER_PLACEHOLDER;
        }

        let elapsedTime = <p/>;
        if (this.state.elapsedTime !== undefined)
        {
            elapsedTime = (
                <p>
                    {`Took ${this.state.elapsedTime}ms to execute`}
                </p>
            );
        }

        return (
            <Container fluid className="d-flex flex-column solution">
                <Row sm={"12"}>
                    <Col sm={"6"}>
                        <Form>
                            <Form.Group>
                                <Form.Label>
                                    <h5>Input</h5>
                                </Form.Label>
                                <Form.Control
                                    as="textarea"
                                    placeholder={placeholder}
                                    value={this.state.input}
                                    onChange={this.handleInputChange}
                                    className="text-monospace"
                                />
                            </Form.Group>
                            <Button
                                variant="primary"
                                onClick={this.handleSubmit}
                            >
                                Solve
                            </Button>
                        </Form>
                    </Col>
                    <Col sm={"6"}>
                        <Form>
                            <Form.Group>
                                <Form.Label>
                                    <h5>Output</h5>
                                </Form.Label>
                                <Form.Control
                                    as="textarea"
                                    value={this.state.output}
                                    className="text-monospace"
                                    readOnly
                                />
                            </Form.Group>
                            {elapsedTime}
                        </Form>
                    </Col>
                </Row>
            </Container>
        );
    }

    public reset(): void
    {
        this.setState({
            instance: this.props.backend(),
            input: "",
            output: "",
            elapsedTime: undefined,
        });
    }

    private handleInputChange(event: any): void
    {
        let input = event.target.value;
        this.setState({input: input});
    }

    private handleSubmit(event: MouseEvent): void
    {
        let { instance, input } = this.state;

        if (instance === undefined)
        {
            alert("Solution backend is undefined");
            return;
        }

        if (input === "")
        {
            input = this.props.placeholder;
        }

        if (input === "")
        {
            alert("Input is empty");
            return;
        }

        let backendState = Object.assign({}, instance.createState(), {
            rawInput: input,
        });
        let output = ""

        let t0 = 0;
        let t1 = 0;
        let elapsedTime = undefined;

        try
        {
            t0 = performance.now();
            backendState = instance.parseInput(backendState);
            backendState = instance.solvePart1(backendState);
            backendState = instance.solvePart2(backendState);
            t1 = performance.now();
            elapsedTime = t1 - t0;

            let { part1Result, part2Result } = backendState;
            if (part1Result === undefined || part2Result === undefined)
            {
                output = "Unable to solve for the given input"
            }
            else
            {
                output = `Part 1: ${part1Result}\nPart 2: ${part2Result}`;
            }

            this.setState({output: output, elapsedTime: elapsedTime});
        }
        catch (e: any)
        {
            alert(e.message);
        }
    }
}


export default SolutionComponent;
