import React from "react";

import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Navbar from "react-bootstrap/Navbar";

import { range } from "../utils/Utils";


type NavigationProps = {
    selected: number;
    handle: (event: string | null) => void;
};


class NavigationComponent extends React.Component<NavigationProps>
{
    render()
    {
        let _selected = this.props.selected.toString();
        let selected = `Day ${_selected.padStart(2, "0")}`;

        return (
            <Navbar>
                <Navbar.Brand
                    className="font-weight-bold"
                >
                    Advent of Code 2020
                </Navbar.Brand>
                <Nav onSelect={this.props.handle}>
                    <NavDropdown
                        title={selected}
                        id="nav-solution-selector"
                    >
                    {range(1, 26).map(i => {
                        return (
                            <NavDropdown.Item key={i} eventKey={i.toString()}>
                                {i.toString().padStart(2, "0")}
                            </NavDropdown.Item>
                        );
                    })}
                    </NavDropdown>
                    <Nav.Link eventKey="reset">Reset</Nav.Link>
                    <Nav.Link
                        eventKey="ignore"
                        href="https://www.gitlab.com/julian-heng/advent-of-code-2020-react"
                    >
                        Source
                    </Nav.Link>
                </Nav>
            </Navbar>
        );
    }
}


export default NavigationComponent;
